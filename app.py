from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin

app = Flask(__name__)

CORS(app)
employees = [
    {"id": 1, "primerApellido": "Sarmiento", "primerNombre":"Nataly", "otrosNombres":"Alejandra", "pais":"Colombia" ,"email":"nataly.sarmiento.1@cidenet.com.co"},
]

@cross_origin
@app.route('/employees', methods=['GET'])
def getEmployee():
    return jsonify(employees)

@cross_origin
@app.route('/employees', methods=['POST'])
def addEmployee():
    primer_apellido = request.json['primerApellido'].lower()
    primer_nombre = request.json['primerNombre'].lower()
    otros_nombres = request.json['otrosNombres'].lower()
    pais = request.json['pais']

    new_id = len(employees) + 1

    if pais.lower() == 'colombia':
        dominio = 'cidenet.com.co'
    elif pais.lower() == 'estados unidos':
        dominio = 'cidenet.com.us'
    else:
        return jsonify({'error': 'Pais invalido'})

    new_employee ={
        "id": new_id,
        "primerApellido": primer_apellido,
        "primerNombre": primer_nombre,
        "otrosNombres": otros_nombres,
        "pais": pais,
        "email": f"{primer_nombre}.{primer_apellido}.{new_id}@{dominio}",
    }

    employees.append(new_employee)
    return jsonify({'message': 'Employee added successfully', 'employee': employees})

@cross_origin
@app.route('/employees/<int:id>', methods=['PUT'])
def editEmployee(id):
    employee = next((e for e in employees if e['id'] == id), None)

    if employee is None:
        return jsonify({'message': f'Employee with id {id} not found'}), 404

    if 'primerApellido' in request.json:
        employee['primerApellido'] = request.json['primerApellido'].lower()
    if 'primerNombre' in request.json:
        employee['primerNombre'] = request.json['primerNombre'].lower()
    if 'otrosNombres' in request.json:
        employee['otrosNombres'] = request.json['otrosNombres'].lower()
    if 'pais' in request.json:
        pais = request.json['pais']
        if pais.lower() == 'colombia':
            email_domain = 'cidenet.com.co'
        elif pais.lower() == 'estados unidos':
            email_domain = 'cidenet.com.us'
        employee['email'] = f"{employee['primerNombre']}.{employee['primerApellido']}.{employee['id']}@{email_domain}"

    return jsonify({'message': 'Employee updated successfully', 'employee': employee})

@cross_origin
@app.route('/employees/<int:id>', methods=['DELETE'])
def deleteEmployee(id):
    employee = next((e for e in employees if e['id'] == id), None)

    if employee is None:
        return jsonify({'message': f'Employee with id {id} not found'}), 404
    
    employees.remove(employee)

    return jsonify({'message': 'Employee deleted successfully', 'employee': employees})

    
if __name__ == '__main__':
    app.run(debug=True)
